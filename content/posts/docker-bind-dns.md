+++
date = "2017-02-27T09:40:43+01:00"
title = "How to setup a local DNS server with BIND in Docker"
draft = false

+++

{{< img-post "http://i.giphy.com" "d3FzEPeFvoFirvIQ.gif" "dns" >}}
<!--more-->

For demo purpose, I would like to have a minimal DNS server locally.

Finally, it's quite easy to setup with Docker and BIND.

Here is the steps:

1. Create a docker-machine 
2. Start a container from [https://hub.docker.com/r/sameersbn/bind/]
3. Setup the DNS server
3. Connect your laptop to the DNS

## Create a docker-machine

> I cannot make it work directly with Docker for Mac. I will investigate this later

Create a docker-machine and connect to it:

```
$ docker-machine create --driver virtualbox bind
Running pre-create checks...
Creating machine...
(bind) Copying /Users/jgsqware/.docker/machine/cache/boot2docker.iso to /Users/jgsqware/.docker/machine/machines/binde/boot2docker.iso...
(bind) Creating VirtualBox VM...
(bind) Creating SSH key...
(bind) Starting the VM...
(bind) Check network to re-create if needed...
(bind) Waiting for an IP...
Waiting for machine to be running, this may take a few minutes...
Detecting operating system of created instance...
Waiting for SSH to be available...
Detecting the provisioner...
Provisioning with boot2docker...
Copying certs to the local machine directory...
Copying certs to the remote machine...
Setting Docker configuration on the remote daemon...
Checking connection to Docker...
Docker is up and running!
To see how to connect your Docker Client to the Docker Engine running on this virtual machine, run: docker-machine env bind

$ eval "$(/usr/local/bin/docker-machine env bind)"
```

## Start a container from [https://hub.docker.com/r/sameersbn/bind/]

```
$ docker run \
      --name bind 
      -it -d \
      --restart=always \
      -p 53:53/udp \
      -p 10000:10000 \
      -v `pwd`/bind:/data \  
      sameersbn/bind:latest
```

## Setup the DNS server

I followed this link to setup the DNS server: [Deploying a DNS Server using Docker](http://www.damagehead.com/blog/2015/04/28/deploying-a-dns-server-using-docker/)

## Connect your laptop to the DNS

{{< img-post "/img" "setup-dns.gif" "dns" >}}